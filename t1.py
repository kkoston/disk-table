def mkhash(s):
    h = 5381

    for c in s:
        h = ((h << 5) + h) + ord(c)

    return h % 100

for w in "to testowe".split(' '):
    print mkhash(w)