import struct
import os

IDX_ITEM_SIZE = 8
INIT_SIZE = 100000

class HashTable:
    def __init__(self, filestore):
        if os.path.exists(filestore):
            fmode = 'r+'
        else:
            fmode = 'w+'
            
        self.f = open(filestore, fmode)
        self.fi = open(filestore + '.idx', fmode)
        
        self.fi.seek(0, os.SEEK_END)
        fi_size = self.fi.tell()
        
        if fi_size == 0:
            self.fi.seek(IDX_ITEM_SIZE * INIT_SIZE - 1)
            self.fi.write('\0') # got to write smth in order to fill the file with zeros
            self.f.write('\0') # nothing can have position 0
            self.idx_size = INIT_SIZE
        else:
            self.idx_size = fi_size / IDX_ITEM_SIZE
    
    def get_item(self, data_pos):
        self.f.seek(data_pos)
        klen, vlen, next_pos = struct.unpack('lll', self.f.read(24))
        key = self.f.read(klen)
        value = self.f.read(vlen)
        
        return key, value, next_pos
    
    def lookup(self, nkey):
        hsh = self.mkhash(nkey)
        
        data_pos = self.get_idx_item(hsh)
        
        if data_pos == 0:
            raise KeyError('Key %s not found' % nkey, None)
        
        while True:
            key, value, next_pos = self.get_item(data_pos)
            if key == nkey:
                return (value, data_pos)
            if next_pos == 0:
                raise KeyError('Key %s not found' % key, data_pos)
            data_pos = next_pos
            
       
    def __setitem__(self, key, val):
        # check if we have it already
        last_item_data_pos = None
        try:
            value, last_item_data_pos = self.lookup(key)
        except KeyError, e:
            if len(e.args) == 2:
                # we have that hash but not the value
                last_item_data_pos = e.args[1]
        else:
            # update the value if different
            if value != val:
                # here logic for updating the item
                pass
            return
        
        # add new item at the end of data file
        hsh = self.mkhash(key)
        self.f.seek(0, os.SEEK_END)
        data_pos = self.f.tell()
        item = struct.pack('lll', len(key), len(val), 0)
        item = item + key + val
        self.f.write(item)

        # if this hash is already in use add at the end of the list (bucket)
        if last_item_data_pos != None:          
            self.f.seek(last_item_data_pos + 16)
            next_pos = struct.pack('l', data_pos)
            self.f.write(next_pos)
        # otherwise add as new hash
        else:
            self.fi.seek(hsh * IDX_ITEM_SIZE)
            idx_item = struct.pack('l', data_pos)
            self.fi.write(idx_item)
        
    def __getitem__(self, key):
        return self.lookup(key)[0]
        
    def get(self, key, ret = None):
        try:
            return self.lookup(key)[0]
        except KeyError:
            return ret 
        
    def mkhash(self, s):
        h = 5381

        for c in s:
            h = ((h << 5) + h) + ord(c)

        return h % self.idx_size
    
    def get_idx_item(self, n):
        self.fi.seek(n * IDX_ITEM_SIZE)
        return struct.unpack('l', self.fi.read(IDX_ITEM_SIZE))[0]      
    
    def dump_idx(self):
        for i in range(0, self.idx_size):
            print i, self.get_idx_item(i)


    
ht = HashTable('db.tab')

ht['me']
    
    
print ht['me']
